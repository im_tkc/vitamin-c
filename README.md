# Eye & Aye
We were tasked to explore uses of computer vision in an interesting way.

Conducting online meeting or classes will never be the same! With Eye & Aye, you will be notified when someone is yawning or sleeping in front of the camera. The program will overlay a **sleeping cloud when one is sleeping** and **vomitting of interesting images from the participant's mouth when he/she is yawning!**

Besides that, sharing some top secret information in front of a webcam? You can **use a predefined item (such as a random membership card) in front of the camera to present** such information using Argumented Reality!

# About Project
This project explicated the following areas of computer vision:

- Face detection + 2D Homography
- Drowsiness Detection (By analysis of the facial features)
- Markerless AR + 2D Homography (Using Homography and Masking images)

Capabilities of the software came mainly from OpenCV Version 3 Library.

# To Install
Before you run the project, you need to issue the following command:

> \> virtualenv venv

> \> venv\Script\activatve

> \> pip install -r requirements

## Dependencies
- openCV 3.0+
- python 3.4+
- numpy
- matplotlib

More dependencies can be found in `requirements.txt`. You can install the dependencies using:

> \> pip install -r requirements.txt

## To Run the Code
At the main directory run following command:

>\> python main.py

# Results

Test Result 1             |  Test Result 2             |  Test Result 3
:-------------------------:|:-------------------------:|:-------------------------:
![](img/result1.jpg "Test result 1")  |  ![](img/result2.jpg "Test result 2")  |  ![](img/result3.jpg "Test result 3")

# Credits
We credit the following projects that contributed towards our submission:

- Augmented Reality with OpenCV
Demo: https://www.youtube.com/watch?v=tYIrvK0O06M
Source: https://www.dropbox.com/sh/lh3vbtcfwli5se3/AABkGm-Cou-5GAOO0a4VPtxGa?dl=0

- Simple Snapchat-like face filters with Python `dlib`, for ETOS Ecosystem.
Source: https://github.com/etosworld/etos-simplefilter

- OpenCV Python Realtime Matching
Demo: https://uklist.info/opencv-python/mKGgk91ozLGpqpY
Source: [https://github.com/dltpdn/opencv-python_edu/blob/master/08.match_track/match_camera.py](https://github.com/dltpdn/opencv-python_edu/blob/master/08.match_track/match_camera.py)

- Drowziness detection
Source: [https://github.com/AkiiSinghal/Drowsiness-Detection](https://github.com/AkiiSinghal/Drowsiness-Detection)
