from tkinter import *
from PIL import Image
from PIL import ImageTk
import cv2, threading, os, time
from threading import Thread
from os import listdir
from os.path import isfile, join
import numpy as np
from math import hypot
import dlib
from imutils import face_utils, rotate_bound
import math
import glob
import copy

### Functions to determine the tiredness of the individual

cap = cv2.VideoCapture(0)
# Filters path
detector = dlib.get_frontal_face_detector()
# Facial landmarks
print("[INFO] loading facial landmark predictor...")
model = "shape_predictor_68_face_landmarks.dat"
predictor = dlib.shape_predictor(model)  # link to model: http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2
#################### START OF FINGER GESTURE ######################
# parameters
# cap_region_x_begin=0.5  # start point/total width
# cap_region_y_end=0.8  # start point/total width
# threshold = 60  #  BINARY threshold
# blurValue = 41  # GaussianBlur parameter
# bgSubThreshold = 50
# learningRate = 0
# count = 0

#variables
# isBgCaptured = 0   # bool, whether the background captured
# triggerSwitch = False  # if true, keyborad simulator works

def printThreshold(thr):
    print("! Changed threshold to "+str(thr))


def removeBG(bgModel, frame, learningRate):
    fgmask = bgModel.apply(frame,learningRate=learningRate)
    # kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    # res = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)

    kernel = np.ones((3, 3), np.uint8)
    fgmask = cv2.erode(fgmask, kernel, iterations=1)
    res = cv2.bitwise_and(frame, frame, mask=fgmask)
    return res


def calculateFingers(res,drawing):  # -> finished bool, cnt: finger count
    #  convexity defect
    hull = cv2.convexHull(res, returnPoints=False)
    if len(hull) > 3:
        defects = cv2.convexityDefects(res, hull)
        if type(defects) != type(None):  # avoid crashing.   (BUG not found)

            cnt = 0
            for i in range(defects.shape[0]):  # calculate the angle
                s, e, f, d = defects[i][0]
                start = tuple(res[s][0])
                end = tuple(res[e][0])
                far = tuple(res[f][0])
                a = math.sqrt((end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2)
                b = math.sqrt((far[0] - start[0]) ** 2 + (far[1] - start[1]) ** 2)
                c = math.sqrt((end[0] - far[0]) ** 2 + (end[1] - far[1]) ** 2)
                angle = math.acos((b ** 2 + c ** 2 - a ** 2) / (2 * b * c))  # cosine theorem
                if angle <= math.pi / 2:  # angle less than 90 degree, treat as fingers
                    cnt += 1
                    cv2.circle(drawing, far, 8, [211, 84, 0], -1)
            return True, cnt
    return False, 0
    
##################### END OF FINGER GESTURE #######################


#################### START OF FINGER GESTURE ######################
# cap.set(10,200)
# cv2.namedWindow('trackbar')
# cv2.createTrackbar('trh1', 'trackbar', threshold, 100, printThreshold)
##################### END OF FINGER GESTURE #######################

def midpoint(p1 ,p2):
    return int((p1.x + p2.x)/2), int((p1.y + p2.y)/2)

font = cv2.FONT_HERSHEY_TRIPLEX

def compute_blinking_ratio(eye_points, facial_landmarks):
    left_point = (facial_landmarks.part(eye_points[0]).x, facial_landmarks.part(eye_points[0]).y)
    right_point = (facial_landmarks.part(eye_points[3]).x, facial_landmarks.part(eye_points[3]).y)
    center_top = midpoint(facial_landmarks.part(eye_points[1]), facial_landmarks.part(eye_points[2]))
    center_bottom = midpoint(facial_landmarks.part(eye_points[5]), facial_landmarks.part(eye_points[4]))

    hor_line_lenght = hypot((left_point[0] - right_point[0]), (left_point[1] - right_point[1]))
    ver_line_lenght = hypot((center_top[0] - center_bottom[0]), (center_top[1] - center_bottom[1]))

    ratio = hor_line_lenght / ver_line_lenght
    return ratio

def compute_mouth_ratio(lips_points, facial_landmarks):
    left_point = (facial_landmarks.part(lips_points[0]).x, facial_landmarks.part(lips_points[0]).y)
    right_point = (facial_landmarks.part(lips_points[2]).x, facial_landmarks.part(lips_points[2]).y)
    center_top = (facial_landmarks.part(lips_points[1]).x, facial_landmarks.part(lips_points[1]).y)
    center_bottom = (facial_landmarks.part(lips_points[3]).x, facial_landmarks.part(lips_points[3]).y)

    hor_line_lenght = hypot((left_point[0] - right_point[0]), (left_point[1] - right_point[1]))
    ver_line_lenght = hypot((center_top[0] - center_bottom[0]), (center_top[1] - center_bottom[1]))
    if hor_line_lenght == 0:
        return ver_line_lenght
    ratio = ver_line_lenght / hor_line_lenght
    return ratio


### Function to set wich sprite must be drawn
def put_sprite(num):
    global SPRITES, BTNS
    SPRITES[num] = (1 - SPRITES[num]) #not actual value
    if SPRITES[num]:
        BTNS[num].config(relief=SUNKEN)
    else:
        BTNS[num].config(relief=RAISED)

# Draws sprite over a image
# It uses the alpha chanel to see which pixels need to be reeplaced
# Input: image, sprite: numpy arrays
# output: resulting merged image
def draw_sprite(frame, sprite, x_offset, y_offset):
    (h,w) = (sprite.shape[0], sprite.shape[1])
    (imgH,imgW) = (frame.shape[0], frame.shape[1])

    if y_offset+h >= imgH: #if sprite gets out of image in the bottom
        sprite = sprite[0:imgH-y_offset,:,:]

    if x_offset+w >= imgW: #if sprite gets out of image to the right
        sprite = sprite[:,0:imgW-x_offset,:]

    if x_offset < 0: #if sprite gets out of image to the left
        sprite = sprite[:,abs(x_offset)::,:]
        w = sprite.shape[1]
        x_offset = 0

    #for each RGB chanel
    for c in range(3):
            #chanel 4 is alpha: 255 is not transpartne, 0 is transparent background
            frame[y_offset:y_offset+h, x_offset:x_offset+w, c] =  \
            sprite[:,:,c] * (sprite[:,:,3]/255.0) +  frame[y_offset:y_offset+h, x_offset:x_offset+w, c] * (1.0 - sprite[:,:,3]/255.0)
    return frame

#Adjust the given sprite to the head's width and position
#in case of the sprite not fitting the screen in the top, the sprite should be trimed
def adjust_sprite2head(sprite, head_width, head_ypos, ontop = True):
    (h_sprite,w_sprite) = (sprite.shape[0], sprite.shape[1])
    factor = 1.0*head_width/w_sprite
    sprite = cv2.resize(sprite, (0,0), fx=factor, fy=factor) # adjust to have the same width as head
    (h_sprite,w_sprite) = (sprite.shape[0], sprite.shape[1])

    y_orig =  head_ypos-h_sprite if ontop else head_ypos # adjust the position of sprite to end where the head begins
    if (y_orig < 0): #check if the head is not to close to the top of the image and the sprite would not fit in the screen
            sprite = sprite[abs(y_orig)::,:,:] #in that case, we cut the sprite
            y_orig = 0 #the sprite then begins at the top of the image
    return (sprite, y_orig)

# Applies sprite to image detected face's coordinates and adjust it to head
def apply_sprite(image, path2sprite,w,x,y, angle, ontop = True):
    sprite = cv2.imread(path2sprite,-1)
    #print sprite.shape
    sprite = rotate_bound(sprite, angle)
    (sprite, y_final) = adjust_sprite2head(sprite, w, y, ontop)
    image = draw_sprite(image,sprite,x, y_final)

def apply_moving_sprite(image, path2sprite,w,x,y, angle, ontop = True):
    sprite = cv2.imread(path2sprite,-1)
    #print sprite.shape
    sprite = rotate_bound(sprite, angle)
    (sprite, y_final) = adjust_sprite2head(sprite, w, y, ontop)
    image = draw_sprite(image,sprite,x, y_final)

#points are tuples in the form (x,y)
# returns angle between points in degrees
def calculate_inclination(point1, point2):
    x1,x2,y1,y2 = point1[0], point2[0], point1[1], point2[1]
    incl = 180/math.pi*math.atan((float(y2-y1))/(x2-x1))
    return incl


def calculate_boundbox(list_coordinates):
    x = min(list_coordinates[:,0])
    y = min(list_coordinates[:,1])
    w = max(list_coordinates[:,0]) - x
    h = max(list_coordinates[:,1]) - y
    return (x,y,w,h)

def get_face_boundbox(points, face_part):
    if face_part == 1:
        (x,y,w,h) = calculate_boundbox(points[17:22]) #left eyebrow
    elif face_part == 2:
        (x,y,w,h) = calculate_boundbox(points[22:27]) #right eyebrow
    elif face_part == 3:
        (x,y,w,h) = calculate_boundbox(points[36:42]) #left eye
    elif face_part == 4:
        (x,y,w,h) = calculate_boundbox(points[42:48]) #right eye
    elif face_part == 5:
        (x,y,w,h) = calculate_boundbox(points[29:36]) #nose
    elif face_part == 6:
        (x,y,w,h) = calculate_boundbox(points[48:68]) #mouth
    return (x,y,w,h)

def calculate_iou(pre_face,cur_face):
    (x1 , y1, width1, height1) = (pre_face.left(), pre_face.top(), pre_face.width(), pre_face.height())
    (x2 , y2, width2, height2) = (cur_face.left(), cur_face.top(), cur_face.width(), cur_face.height())
    endx = max(x1+width1,x2+width2)
    startx = min(x1,x2)
    width = width1+width2-(endx-startx)
    endy = max(y1+height1,y2+height2)
    starty = min(y1,y2)
    height = height1+height2-(endy-starty)
    if width <=0 or height <= 0:
        ratio = 0
    else:
        area = width*height
        area1 = width1*height1
        area2 = width2*height2
        ratio = area*1./(area1+area2-area)
    return ratio

iou_thres=0.93
#Principal Loop where openCV (magic) ocurs
def cvloop(run_event):
    global panelA
    global SPRITES
    dir_ = "./sprites/flyes/"
    dir_1 = "./sprites/Prof/"
    flies = [f for f in listdir(dir_) if isfile(join(dir_, f))] #image of flies to make the "animation"
#    Prof = [f for f in listdir(dir_1) if isfile(join(dir_1, f))]

    # parameters
    cap_region_x_begin=0.5  # start point/total width
    cap_region_y_end=0.8  # start point/total width
    threshold = 60  #  BINARY threshold
    blurValue = 41  # GaussianBlur parameter
    bgSubThreshold = 50
    learningRate = 0
    count = 0

    # variables
    isBgCaptured = 0   # bool, whether the background captured
    triggerSwitch = False  # if true, keyborad simulator works
    
    zoomLevel = 0   #when zoomLevel is positive it zooms in, when its negative it zooms out
    processing = True   #boolean variable using for disabling the image processing
    maskThreshold = threshold

    i = 0
    # video_capture = cv2.VideoCapture(0) #read from webcam
    # cap = video_capture
    
    # Start of video capture and setting the frame size
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
    
    cap.set(10,200)
    cv2.namedWindow('trackbar')
    cv2.namedWindow('trackbar', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('trackbar', 300,5)
    cv2.createTrackbar('trh1', 'trackbar', threshold, 100, printThreshold)
    
    (x,y,w,h) = (0,0,10,10) #whatever initial values
    pre_face = 0
    img1 = cv2.imread('koi.png') #Insert image that will be masked over
    win_name = 'Camera Matching'
    MIN_MATCH = 10
    images = glob.glob('*.JPG') #all the jpg images in the folder could be displayed
    currentImage=0  #the first image is selected
    replaceImg=cv2.imread(images[currentImage])
    rows,cols,ch = replaceImg.shape
    pts1 = np.float32([[0, 0],[0,rows],[(cols),(rows)],[cols,0]])    #this points are necesary for the transformation
    
    # Detector1 used runs ORB (Oriented FAST and Rotated BRIEF)
    detector1 = cv2.ORB_create(1000)
    # Flann for approximating the nearest neighbour
    FLANN_INDEX_LSH = 6
    index_params= dict(algorithm = FLANN_INDEX_LSH,
                       table_number = 6,
                       key_size = 12,
                       multi_probe_level = 1)
    search_params=dict(checks=32)
    matcher = cv2.FlannBasedMatcher(index_params, search_params)

    cant = 0
    while run_event.is_set(): #while the thread is active we loop
        ret2, frame = cap.read()
        ret, image = cap.read()
        #image = cv2.flip(image, 1)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = detector(gray,0) 

        #ret_chessboard, corners = cv2.findChessboardCorners(gray, (9,6),None)

        # Detection of the tiredness
        count = 0

        #  frame = cv2.flip(frame,0)
        #  gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        for face in faces:
            landmarks = predictor(gray, face)
            left_eye_ratio = compute_blinking_ratio([36, 37, 38, 39, 40, 41], landmarks)
            right_eye_ratio = compute_blinking_ratio([42, 43, 44, 45, 46, 47], landmarks)
            blinking_ratio = (left_eye_ratio + right_eye_ratio) / 2
            cv2.putText(image, str(blinking_ratio), (0, 13), font, 0.5, (100, 100, 100))
            print(left_eye_ratio, right_eye_ratio, blinking_ratio)

            inner_lip_ratio = compute_mouth_ratio([60, 62, 64, 66], landmarks)
            outter_lip_ratio = compute_mouth_ratio([48, 51, 54, 57], landmarks)
            mouth_opening_ratio = (inner_lip_ratio + outter_lip_ratio) / 2;
            cv2.putText(image, str(mouth_opening_ratio), (448, 13), font, 0.5, (100, 100, 100))
            print(inner_lip_ratio, outter_lip_ratio, mouth_opening_ratio)

            if mouth_opening_ratio > 0.38 and blinking_ratio > 4 or blinking_ratio > 4.3:
                count = count + 1
            else:
                count = 0
            (x,y,w,h) = (face.left(), face.top(), face.width(), face.height())
            # for face in faces: #if there are faces
            if len(faces) > 0:  # only one face for simplicity
                cur_face = faces[0]
                if (pre_face != 0):
                    c_iou = calculate_iou(pre_face, cur_face)
                    if (c_iou > iou_thres):
                        cur_face = pre_face
                pre_face = cur_face
                face = cur_face

# *** Facial Landmarks detection
            shape = predictor(gray, face)
            shape = face_utils.shape_to_np(shape)
            (a0, b0, c0, d0) = get_face_boundbox(shape, 6)
            #cv2.rectangle(frame, (a0, b0), (a0 + c0, b0 + d0+100), (0, 0, 255), 2)
            #cv2.imshow("Frame", frame)
            incl = calculate_inclination(shape[17], shape[26])  # inclination based on eyebrows

# condition to see if mouth is open
            is_mouth_open = (shape[66][1] - shape[62][1]) >= 20  # y coordiantes of landmark points of lips
#            else:
#                cv2.rectangle(frame, (x, y), (w, h), (0, 255, 0), 2)

            # hat condition
            # if SPRITES[0]:
                # apply_sprite(image, "./sprites/hat.png", w, x, y, incl)
         
            (x0, y0, w0, h0) = get_face_boundbox(shape, 6)  # bound box of mouth
            i=0
            if is_mouth_open:
                print("mouth!")
                (x0, y0, w0, h0) = get_face_boundbox(shape, 6)
                for j in range(0, 5):
                    dist = j * 30
                    #apply_moving_sprite(image, "./sprites/prof.png", w0, x0, y0+dist, incl, ontop=False)
                    apply_sprite(image, dir_ + flies[i], w0, x0 , y0 +dist, h0 + dist,ontop=False)
                    i += 1
                    i = 0 if i >= len(flies) else i
            #sleeping icon
            if mouth_opening_ratio > 0.38 and blinking_ratio > 4 or blinking_ratio > 4.3:
                #cv2.rectangle(frame, (X_0, Y_0, (X_1, Y_1), (0, 0, 255), 2)
#                cv2.putText(frame, "Drowsing", (X_0, Y_0 - 5), font, 0.5, (0, 0, 255))
#                (x7, y7, w7, h7) = get_face_boundbox(shape, 7)
                apply_sprite(image, "./sprites/dreaming.png", w, x, y, incl, ontop=True)
            # If chessboard found, do the processing
            
        #################### START OF FINGER GESTURE ######################
        cnt = -1
        isFinishCal = False

        image = cv2.bilateralFilter(image, 5, 50, 100)  # smoothing filter
        #frame = cv2.flip(frame, 1)  # flip the frame horizontally
        #cv2.rectangle(image, (int(cap_region_x_begin * image.shape[1]), 0),
        #            (image.shape[1], int(cap_region_y_end * image.shape[0])), (255, 0, 0), 2)
        # cv2.imshow('original', frame)

        #  Main operation
        if isBgCaptured == 1:  # this part wont run until background captured
            img = removeBG(bgModel, image, learningRate)
            img = img[0:int(cap_region_y_end * image.shape[0]),
                        int(cap_region_x_begin * image.shape[1]):image.shape[1]]  # clip the ROI
            #cv2.imshow('mask', img)

            # convert the image into binary image
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            blur = cv2.GaussianBlur(gray, (blurValue, blurValue), 0)
            #cv2.imshow('blur', blur)
            ret, thresh = cv2.threshold(blur, threshold, 255, cv2.THRESH_BINARY)
            #cv2.imshow('ori', thresh)


            # get the coutours
            thresh1 = copy.deepcopy(thresh)
            contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2:]
            length = len(contours)
            maxArea = -1
            if length > 0:
                for i in range(length):  # find the biggest contour (according to area)
                    temp = contours[i]
                    area = cv2.contourArea(temp)
                    if area > maxArea:
                        maxArea = area
                        ci = i

                res = contours[ci]
                hull = cv2.convexHull(res)
                drawing = np.zeros(img.shape, np.uint8)
                cv2.drawContours(drawing, [res], 0, (0, 255, 0), 2)
                cv2.drawContours(drawing, [hull], 0, (0, 0, 255), 3)

                isFinishCal,cnt = calculateFingers(res,drawing)
                print(cnt)
                if triggerSwitch is True:
                    if isFinishCal is True and cnt <= 2:
                        print(cnt)
                        #app('System Events').keystroke(' ')  # simulate pressing blank space


            #cv2.imshow('output', drawing)

        #Keyboard OP
        k = cv2.waitKey(10)
        if k == 27:  # press ESC to exit
            video_capture.release()
            cv2.destroyAllWindows()
            break
        elif k == ord('b'):  # press 'b' to capture the background
            bgModel = cv2.createBackgroundSubtractorMOG2(0, bgSubThreshold)
            isBgCaptured = 1
            print( '!!!Background Captured!!!')
        elif k == ord('r'):  # press 'r' to reset the background
            bgModel = None
            triggerSwitch = False
            isBgCaptured = 0
            print ('!!!Reset BackGround!!!')
        elif k == ord('t'):
            triggerSwitch = True
            print ('!!!Trigger On!!!')
        print(count)
        if cant == 15:
            bgModel = cv2.createBackgroundSubtractorMOG2(0, bgSubThreshold)
            isBgCaptured = 1
            print('!!!Background Captured!!!')
            cant=0

        ##################### END OF FINGER GESTURE ########################

        # If found, do the processing
        if img1 is None:  # 등록된 이미지 없음, 카메라 바이패스
            res = frame
        else:             # 등록된 이미지 있는 경우, 매칭 시작
            img2 = frame
            gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
            gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
            # 키포인트와 디스크립터 추출
            kp1, desc1 = detector1.detectAndCompute(gray1, None)
            kp2, desc2 = detector1.detectAndCompute(gray2, None)
            # k=2로 knnMatch
            matches = matcher.knnMatch(desc1, desc2, 2)
            # 이웃 거리의 75%로 좋은 매칭점 추출---②
            ratio = 0.75
            good_matches = [m[0] for m in matches \
                                if len(m) == 2 and m[0].distance < m[1].distance * ratio]
            #print('good matches:%d/%d' %(len(good_matches),len(matches)))
            # 모든 매칭점 그리지 못하게 마스크를 0으로 채움
            matchesMask = np.zeros(len(good_matches)).tolist()
            if len(good_matches) > MIN_MATCH:
                src_pts = np.float32([ kp1[m.queryIdx].pt for m in good_matches ])
                dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good_matches ])
                mtrx, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
                mask = cv2.erode(mask, (3, 3))
                mask = cv2.dilate(mask, (3, 3))
                if mask.sum() > MIN_MATCH:
                    # run this function if the mask is accurate
                    matchesMask = mask.ravel().tolist()
                    # mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
                    h,w, = img1.shape[:2]
                    pts = np.float32([ [[0,0]],[[0,h-1]],[[w-1,h-1]],[[w-1,0]] ])
                    dst = cv2.perspectiveTransform(pts,mtrx)
                    #Example of dst output
                    #<class 'numpy.ndarray'>
                    #[[[433.6934  283.07095]]
                    #[[432.0508  283.20688]]
                    #[[433.5561  283.08893]]
                    #[[432.81674 283.13977]]]
                    dst = cv2.getPerspectiveTransform(pts1,dst)
                    rows, cols, ch = frame.shape
                    distance = cv2.warpPerspective(replaceImg,dst,(cols,rows))
                    #img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)
                    rt, mk = cv2.threshold(cv2.cvtColor(distance, cv2.COLOR_BGR2GRAY), maskThreshold, 1,cv2.THRESH_BINARY_INV)
                    mk = cv2.erode(mk, (3, 3))
                    mk = cv2.dilate(mk, (3, 3))
                    for c in range(0, 3):
                        image[:, :, c] = distance[:,:,c]*(1-mk[:,:]) + frame[:,:,c]*mk[:,:]
                        #dst_c[:,:,c](1 - mask[:, :]) + frame[:, :, c] * mask[:, :]
      #              #        cv2.imshow('mask',mask*255)
                    # finally the result is presented
                #cv2.imshow('img', image)


            #Wait for the key
            # k = cv2.waitKey(10)
            key = cv2.waitKey(1)
            #decide the action based on the key value (quit, zoom, change image)
            if key == ord('q'): # quit
                print ('Quit')
                break
            if key == ord('i'): # + zoom in
                zoomLevel=zoomLevel+0.05
                rows,cols,ch = replaceImg.shape
                A=[-zoomLevel*cols,-zoomLevel*rows]
                B=[-zoomLevel*cols,zoomLevel*rows]
                C=[zoomLevel*cols,zoomLevel*rows]
                D=[zoomLevel*cols,-zoomLevel*rows]
                pts1 = np.float32([[0, 0],[0,rows],[(cols),(rows)],[cols,0]])
                pts1 = pts1 + np.float32([A,B,C,D])
                print ('Zoom in')
            cant += 1
            if isFinishCal:
                print("It's supposed to work mah")
            if cnt>0:
                print('cnt also can arhhh')
                print(zoomLevel)

            if ret and isFinishCal and cnt>=4 and zoomLevel>-0.2:
                zoomLevel=zoomLevel-0.05
                rows,cols,ch = replaceImg.shape
                pts1 = np.float32([[0, 0],[0,rows],[(cols),(rows)],[cols,0]])
                pts1 = pts1 + np.float32([[-zoomLevel*cols,-zoomLevel*rows],
                                          [-zoomLevel*cols,zoomLevel*rows],
                                          [zoomLevel*cols,zoomLevel*rows],
                                          [zoomLevel*cols,-zoomLevel*rows]])
                print ('Zoom out')
            if key == ord('n'): # -> next image
                if currentImage<len(images)-1:
                    currentImage=currentImage+1
                    replaceImg=cv2.imread(images[currentImage])
                    rows, cols, ch = replaceImg.shape
                    A = [-zoomLevel * cols, -zoomLevel * rows]
                    B = [-zoomLevel * cols, zoomLevel * rows]
                    C = [zoomLevel * cols, zoomLevel * rows]
                    D = [zoomLevel * cols, -zoomLevel * rows]
                    pts1 = np.float32([[0, 0], [0, rows], [(cols), (rows)], [cols, 0]])
                    pts1 = pts1 + np.float32([A, B, C, D])
                    print ('Next image')
                else:
                    print ('No more images on the right')
            if key == ord('m'): # <- previous image
                if currentImage>0:
                    currentImage=currentImage-1
                    replaceImg=cv2.imread(images[currentImage])
                    rows, cols, ch = replaceImg.shape
                    A = [-zoomLevel * cols, -zoomLevel * rows]
                    B = [-zoomLevel * cols, zoomLevel * rows]
                    C = [zoomLevel * cols, zoomLevel * rows]
                    D = [zoomLevel * cols, -zoomLevel * rows]
                    pts1 = np.float32([[0, 0], [0, rows], [(cols), (rows)], [cols, 0]])
                    pts1 = pts1 + np.float32([A, B, C, D])
                    print ('Previous image')
                else:
                    print ('No more images on the left')

        # OpenCV represents image as BGR; PIL but RGB, we need to change the chanel order
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # conerts to PIL format
        image = Image.fromarray(image)
        # Converts to a TK format to visualize it in the GUI
        image = ImageTk.PhotoImage(image)
        # Actualize the image in the panel to show it
        panelA.configure(image=image)
        panelA.image = image
        
        
    video_capture.release()

# Initialize GUI object
root = Tk()
root.title("Snap chat filters")
this_dir = os.path.dirname(os.path.realpath(__file__))

##Create 5 buttons and assign their corresponding function to active sprites
# btn1 = Button(root, text="Hat", command = lambda: put_sprite(0))
# btn1.pack(side="top", fill="both", expand="no", padx="5", pady="5")

# Create the panel where webcam image will be shown
panelA = Label(root)
panelA.pack( padx=10, pady=10)

# Variable to control which sprite you want to visualize
# SPRITES = [0,0,0,0,0] #hat, mustache, flies, glasses, doggy -> 1 is visible, 0 is not visible
# BTNS = [btn1, btn2, btn3, btn4, btn5]

# Creates a thread where the magic ocurs
run_event = threading.Event()
run_event.set()
action = Thread(target=cvloop, args=(run_event,))
action.setDaemon(True)
action.start()

# Function to close all properly, aka threads and GUI
def terminate():
   global root, run_event, action
   print("Closing thread opencv...")
   run_event.clear()
   time.sleep(1)
   #action.join() #strangely in Linux this thread does not terminate properly, so .join never finishes
   root.destroy()
   print("All closed! Chao")

# When the GUI is closed it actives the terminate function
root.protocol("WM_DELETE_WINDOW", terminate)
root.mainloop() #creates loop of GUI





